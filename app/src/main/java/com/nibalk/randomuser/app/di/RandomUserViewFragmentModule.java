package com.nibalk.randomuser.app.di;

import android.arch.lifecycle.ViewModelProvider;

import com.nibalk.framework.base.viewmodel.ViewModelProviderFactory;
import com.nibalk.framework.security.encryption.manager.EncryptionManager;
import com.nibalk.framework.storage.roomdb.manager.RoomDbManager;
import com.nibalk.randomuser.app.data.repository.RandomUserRepository;
import com.nibalk.randomuser.app.view.fragment.RandomUserViewFragment;
import com.nibalk.randomuser.app.viewmodel.RandomUserViewFragmentViewModel;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

@Module
public class RandomUserViewFragmentModule {

    @Module
    public static abstract class RandomUserViewFragmentProvider {
        @ContributesAndroidInjector(modules = RandomUserViewFragmentModule.class)
        abstract RandomUserViewFragment provideRandomUserViewFragment();
    }

    @Provides
    RandomUserViewFragmentViewModel randomUserViewFragmentViewModel(RandomUserRepository repository,
                                                                    RoomDbManager roomDbManager) {
        return new RandomUserViewFragmentViewModel(repository, roomDbManager);
    }

    @Provides
    @Named("FRAGMENT")
    ViewModelProvider.Factory provideRandomUserViewFragmentViewModel(RandomUserViewFragmentViewModel viewModel) {
        return new ViewModelProviderFactory<>(viewModel);
    }
}
