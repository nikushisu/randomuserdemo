package com.nibalk.randomuser.app.data.service;

import com.nibalk.randomuser.app.data.model.RandomUserResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

import static com.nibalk.randomuser.app.utils.Constants.AppUrls.BASE_URL;
import static com.nibalk.randomuser.app.utils.Constants.Keys.QUERY_PARAMS_GENDER_LABEL;
import static com.nibalk.randomuser.app.utils.Constants.Keys.QUERY_PARAMS_RESULTS_LABEL;
import static com.nibalk.randomuser.app.utils.Constants.Keys.QUERY_PARAMS_SEED_LABEL;

public interface RandomUserService {

    @GET(BASE_URL)
    Call<RandomUserResponse> fetchUserByGender(@Query(QUERY_PARAMS_GENDER_LABEL) String gender);

    @GET(BASE_URL)
    Call<RandomUserResponse> fetchUserBySeed(@Query(QUERY_PARAMS_SEED_LABEL) String seed);

    @GET(BASE_URL)
    Call<RandomUserResponse> fetchUsers(@Query(QUERY_PARAMS_RESULTS_LABEL) String count);

}
