package com.nibalk.randomuser.app.view.activity;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.View;
import android.widget.TextView;

import com.nibalk.framework.base.view.BaseActivity;
import com.nibalk.framework.security.biometric.manager.BiometricManager;
import com.nibalk.framework.security.biometric.utils.FingerprintManagerCallbacks;
import com.nibalk.framework.security.biometric.utils.FingerprintSupportStatus;
import com.nibalk.randomuser.R;
import com.nibalk.randomuser.app.view.NavigationController;
import com.nibalk.randomuser.app.viewmodel.MainActivityViewModel;
import com.nibalk.randomuser.databinding.ActivityMainBinding;

import javax.inject.Inject;

import timber.log.Timber;

public class MainActivity extends BaseActivity<MainActivityViewModel, ActivityMainBinding> {

    private static final String TAG = "MainActivity";

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Inject
    NavigationController navigationController;

    @Inject
    BiometricManager biometricManager;

    private ActivityMainBinding binding;
    private MainActivityViewModel viewModel;

    private TextView tvMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initScreen();
        startFingerprintAuthentication();
    }

    @Override
    protected MainActivityViewModel setupViewModel() {
        Timber.d( "setupViewModel");
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainActivityViewModel.class);
        return viewModel;
    }

    @Override
    protected ActivityMainBinding setupDataBinding() {
        Timber.d("setupDataBinding");
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        return binding;
    }

    private void initScreen() {
        tvMessage = findViewById(R.id.tvMessage);
        tvMessage.setText("");

        findViewById(R.id.tvProceedLink).setOnClickListener(view -> {
            navigationController.loadRandomUserActivity();
            finish();
        });

    }
    private void startFingerprintAuthentication() {
        FingerprintSupportStatus status = biometricManager.isDeviceSupportedFingerprint(this);

        if (status == FingerprintSupportStatus.FP_SUPPORTED) {
            biometricManager.startFingerprintAuthentication(this, fingerprintEvents);
        } else {
            displayDeviceUnsupportedMessage(status);
        }
    }

    private void displayDeviceUnsupportedMessage(FingerprintSupportStatus status) {
        String message = "";
        switch (status) {
            case FP_HARDWARE_NOT_SUPPORTED:
                message = "Your device doesn't have fingerprint sensor";
                break;
            case FP_HAS_NOT_REGISTERED:
                message = "Your Device has no registered Fingerprints. Please register at least one in your Device settings";
                break;
            case FP_LOCK_SCREEN_NOT_SECURED:
                message = "Please enable lockscreen security in your device's Settings";
                break;
            case FP_PERMISSION_NOT_GRANTED:
                message = "Please enable the fingerprint permission";
                break;
            case FP_NOT_SUPPORTED:
            default:
                message = "Your device doesn't support fingerprint authentication";
                break;
        }
        tvMessage.setText(message);
    }

    private FingerprintManagerCallbacks fingerprintEvents = new FingerprintManagerCallbacks() {
        @Override
        public void onAuthenticationError(int errorCode, CharSequence errString) {
            tvMessage.setText(errString);
        }

        @Override
        public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
            tvMessage.setText(helpString);
        }

        @Override
        public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
            navigationController.loadRandomUserActivity();
            finish();
        }

        @Override
        public void onAuthenticationFailed() {
            tvMessage.setText("Your device doesn't support fingerprint authentication");

        }
    };

}
