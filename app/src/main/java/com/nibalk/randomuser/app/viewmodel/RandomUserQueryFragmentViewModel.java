package com.nibalk.randomuser.app.viewmodel;

import com.nibalk.framework.base.error.BaseException;
import com.nibalk.framework.base.viewmodel.BaseViewModel;
import com.nibalk.framework.base.viewmodel.SingleLiveEvent;
import com.nibalk.randomuser.app.data.model.RandomUser;
import com.nibalk.randomuser.app.data.model.RandomUserInfo;
import com.nibalk.randomuser.app.data.model.RandomUserResponse;
import com.nibalk.randomuser.app.data.repository.RandomUserRepository;


import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class RandomUserQueryFragmentViewModel extends BaseViewModel {

    private RandomUserRepository randomUserRepository;

    private SingleLiveEvent<BaseException> randomUserError = new SingleLiveEvent<>();
    private SingleLiveEvent<RandomUser> randomUser = new SingleLiveEvent<>();
    private SingleLiveEvent<List<RandomUser>> randomUsers = new SingleLiveEvent<>();


    public RandomUserQueryFragmentViewModel(RandomUserRepository randomUserRepository) {
        this.randomUserRepository = randomUserRepository;
    }

    public void getRandomUserByGender(String selectedGender) {
        Timber.d("getRandomUserByGender()");

        Disposable disposable = randomUserRepository.getRandomUserByGender(selectedGender)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    Timber.d("getQueryUserSuccess (byGender) - response");
                    onResponseReceived(response);
                }, throwable -> {
                    Timber.d("getQueryUserSuccess (byGender) - error");
                    if (throwable instanceof BaseException) {
                        onQueryUserFail((BaseException) throwable);
                    } else {
                        onQueryUserFail(new BaseException("Error", throwable.getMessage()));
                    }
                });
        getCompositeDisposable().add(disposable);
    }

    public void getRandomUserByUserId(String userId) {
        Timber.d("getRandomUserByUserId()");

        Disposable disposable = randomUserRepository.getRandomUserBySeed(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    Timber.d("getQueryUserSuccess (byUserID) - response");
                    onResponseReceived(response);
                }, throwable -> {
                    Timber.d("getQueryUserSuccess (byUserID) - error");
                    if (throwable instanceof BaseException) {
                        onQueryUserFail((BaseException) throwable);
                    } else {
                        onQueryUserFail(new BaseException("Error", throwable.getMessage()));
                    }
                });
        getCompositeDisposable().add(disposable);
    }

    public void getRandomUsersByCount(String count) {
        Timber.d("getRandomUsers()");

        Disposable disposable = randomUserRepository.getRandomUsers(count)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    Timber.d("getQueryUserSuccess (byUserCount) - response");
                    onResponseReceived(response);
                }, throwable -> {
                    Timber.d("getQueryUserSuccess (byUserCount) - error");
                    if (throwable instanceof BaseException) {
                        onQueryUserFail((BaseException) throwable);
                    } else {
                        onQueryUserFail(new BaseException("Error", throwable.getMessage()));
                    }
                });
        getCompositeDisposable().add(disposable);
    }

    private void onResponseReceived(RandomUserResponse response) {
        Timber.d("getQueryUserSuccess - response");

        if (response != null) {
            Timber.d("getQueryUserSuccess - response success -> %s", response.toString());

            List<RandomUser> users = response.getRandomUsers();
            RandomUserInfo info = response.getRandomUserInfo();

            if (users != null && users.size() > 0) {
                Timber.d("getQueryUserSuccess - user count -> %d", users.size());
                Timber.d("getQueryUserSuccess - user list -> %s", users.toString());

                if (users.size() == 1) {
                    RandomUser user = users.get(0);

                    if (info != null) {
                        Timber.d("getQueryUserSuccess - user info -> %s", info.toString());
                        user.setSeed(info.getSeed());
                    }

                    Timber.d("getQueryUserSuccess - user -> %s", user.toString());
                    onQueryUserSuccess(user);
                } else {
                    onQueryUserSuccess(users);
                }
            } else {
                onQueryUserFail(new BaseException("No user data"));
            }
        } else {
            Timber.d("getQueryUserSuccess - response fail");
            onQueryUserFail(new BaseException("No data received"));
        }
    }


    public SingleLiveEvent<RandomUser> getQueryUserSuccess() {
        return this.randomUser;
    }

    private void onQueryUserSuccess(RandomUser randomUser) {
        this.randomUser.setValue(randomUser);
    }

    public SingleLiveEvent<List<RandomUser>> getQueryUsersSuccess() {
        return randomUsers;
    }

    private void onQueryUserSuccess(List<RandomUser> randomUsers) {
        this.randomUsers.setValue(randomUsers);
    }

    public SingleLiveEvent<BaseException> getQueryUserFail() {
        return randomUserError;
    }

    private void onQueryUserFail(BaseException randomUserError) {
        this.randomUserError.setValue(randomUserError);
    }

}
