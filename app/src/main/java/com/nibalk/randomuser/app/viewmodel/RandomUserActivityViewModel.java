package com.nibalk.randomuser.app.viewmodel;

import com.nibalk.framework.base.viewmodel.BaseViewModel;
import com.nibalk.randomuser.app.data.repository.RandomUserRepository;

public class RandomUserActivityViewModel extends BaseViewModel {

    private RandomUserRepository repository;

    public RandomUserActivityViewModel(RandomUserRepository repository) {
        this.repository = repository;
    }
}
