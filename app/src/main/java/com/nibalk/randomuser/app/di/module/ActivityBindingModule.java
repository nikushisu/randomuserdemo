package com.nibalk.randomuser.app.di.module;

import com.nibalk.framework.base.di.annotation.ActivityScope;
import com.nibalk.randomuser.app.di.MainActivityModule;
import com.nibalk.randomuser.app.di.RandomUserActivityModule;
import com.nibalk.randomuser.app.di.RandomUserListFragmentModule;
import com.nibalk.randomuser.app.di.RandomUserQueryFragmentModule;
import com.nibalk.randomuser.app.di.RandomUserViewFragmentModule;
import com.nibalk.randomuser.app.view.activity.MainActivity;
import com.nibalk.randomuser.app.view.activity.RandomUserActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = {RandomUserActivityModule.class,
            RandomUserQueryFragmentModule.RandomUserQueryFragmentProvider.class,
            RandomUserViewFragmentModule.RandomUserViewFragmentProvider.class,
            RandomUserListFragmentModule.RandomUserListFragmentProvider.class})
    abstract RandomUserActivity bindingRandomUserActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = {MainActivityModule.class})
    abstract MainActivity bindingMainActivity();
}
