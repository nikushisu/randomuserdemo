package com.nibalk.randomuser.app.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RandomUserInfo {

    @Expose
    @SerializedName("version")
    private String version;
    @Expose
    @SerializedName("page")
    private int page;
    @Expose
    @SerializedName("results")
    private int results;
    @Expose
    @SerializedName("seed")
    private String seed;

    public RandomUserInfo() {}

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getResults() {
        return results;
    }

    public void setResults(int results) {
        this.results = results;
    }

    public String getSeed() {
        return seed;
    }

    public void setSeed(String seed) {
        this.seed = seed;
    }
}
