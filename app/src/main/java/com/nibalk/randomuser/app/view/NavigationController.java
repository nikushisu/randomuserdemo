package com.nibalk.randomuser.app.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import com.nibalk.framework.base.view.BaseNavigationController;
import com.nibalk.randomuser.R;
import com.nibalk.randomuser.app.data.model.RandomUser;
import com.nibalk.randomuser.app.view.activity.MainActivity;
import com.nibalk.randomuser.app.view.activity.RandomUserActivity;
import com.nibalk.randomuser.app.view.fragment.RandomUserListFragment;
import com.nibalk.randomuser.app.view.fragment.RandomUserQueryFragment;
import com.nibalk.randomuser.app.view.fragment.RandomUserViewFragment;

public class NavigationController extends BaseNavigationController {

    private Context context;
    private FragmentManager fragmentManager;


    public NavigationController(MainActivity activity) {
        super(activity, activity.getSupportFragmentManager());
        this.context = activity;
        this.fragmentManager = activity.getSupportFragmentManager();
    }

    public NavigationController(RandomUserActivity activity) {
        super(activity, activity.getSupportFragmentManager());
        this.context = activity;
        this.fragmentManager = activity.getSupportFragmentManager();
    }

    public void loadRandomUserActivity() {
        context.startActivity(new Intent(context, RandomUserActivity.class));
    }

    public void popBackStack() {
        fragmentManager.popBackStack();
    }

    public void loadRandomUserQueryFragment(Bundle bundle) {
        replaceFragment(bundle, RandomUserQueryFragment.newInstance(),
                RandomUserQueryFragment.class, R.id.content_frame);
    }

    public void loadRandomUserViewFragment(Bundle bundle) {
        addFragment(bundle, RandomUserViewFragment.newInstance(),
                RandomUserViewFragment.class, R.id.content_frame);
    }

    public void loadRandomUserListFragment(Bundle bundle) {
        replaceFragment(bundle, RandomUserListFragment.newInstance(),
                RandomUserListFragment.class, R.id.content_frame);
    }

}
