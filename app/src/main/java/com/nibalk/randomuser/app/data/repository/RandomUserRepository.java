package com.nibalk.randomuser.app.data.repository;

import com.nibalk.randomuser.app.data.model.RandomUser;
import com.nibalk.randomuser.app.data.model.RandomUserResponse;

import java.util.List;

import io.reactivex.Observable;

public interface RandomUserRepository {

    Observable<RandomUserResponse> getRandomUserByGender(String gender);

    Observable<RandomUserResponse> getRandomUserBySeed(String seed);

    Observable<RandomUserResponse> getRandomUsers(String count);

    Observable<Long> insertRandomUser(RandomUser count);

    Observable<List<RandomUser>> selectAllRandomUsers();

    Observable<Integer> deleteRandomUser(int userId);

}
