package com.nibalk.randomuser.app.di;

import android.arch.lifecycle.ViewModelProvider;
import android.content.Context;

import com.nibalk.framework.base.di.annotation.ActivityScope;
import com.nibalk.framework.base.viewmodel.ViewModelProviderFactory;
import com.nibalk.randomuser.app.data.repository.RandomUserRepository;
import com.nibalk.randomuser.app.view.NavigationController;
import com.nibalk.randomuser.app.view.activity.RandomUserActivity;
import com.nibalk.randomuser.app.viewmodel.RandomUserActivityViewModel;

import dagger.Module;
import dagger.Provides;

@Module
public class RandomUserActivityModule {

    @Provides
    RandomUserActivityViewModel randomUserViewModel(RandomUserRepository repository) {
        return new RandomUserActivityViewModel(repository);
    }

    @Provides
    ViewModelProvider.Factory provideMainActivityViewModel(RandomUserActivityViewModel viewModel) {
        return new ViewModelProviderFactory<>(viewModel);
    }

    @Provides
    @ActivityScope
    public NavigationController provideNavigationController(RandomUserActivity activity) {
        return new NavigationController(activity);
    }

}
