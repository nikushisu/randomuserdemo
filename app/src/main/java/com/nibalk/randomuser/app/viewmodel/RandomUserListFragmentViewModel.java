package com.nibalk.randomuser.app.viewmodel;

import com.nibalk.framework.base.error.BaseException;
import com.nibalk.framework.base.viewmodel.SingleLiveEvent;
import com.nibalk.framework.storage.roomdb.manager.RoomDbManager;
import com.nibalk.randomuser.app.data.model.RandomUser;
import com.nibalk.randomuser.app.data.repository.RandomUserRepository;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class RandomUserListFragmentViewModel extends RandomUserFragmentBaseViewModel {

    private RandomUserRepository randomUserRepository;
    private RoomDbManager roomDbManager;

    private SingleLiveEvent<BaseException> dbQueryAllFail = new SingleLiveEvent<>();
    private SingleLiveEvent<List<RandomUser>> dbQueryAllSuccess = new SingleLiveEvent<>();


    public RandomUserListFragmentViewModel(RandomUserRepository randomUserRepository,
                                           RoomDbManager roomDbManager) {
        this.randomUserRepository = randomUserRepository;
        this.roomDbManager = roomDbManager;
    }

    public void queryStoredRandomUser() {
        Timber.d("queryStoredRandomUser()");

        Disposable disposable = randomUserRepository.selectAllRandomUsers()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    Timber.d("queryStoredRandomUser - response before decrypt %s", response);
                    response = decryptRandomUserList(roomDbManager, response);
                    Timber.d("queryStoredRandomUser - response after decrypt %s", response);
                    onDbQueryAllSuccess(response);
                }, throwable -> {
                    Timber.d("queryStoredRandomUser - error");
                    onDbQueryAllFail(new BaseException(throwable));
                });
        getCompositeDisposable().add(disposable);
    }

    public SingleLiveEvent<BaseException> getDbQueryAllFail() {
        return dbQueryAllFail;
    }

    private void onDbQueryAllFail(BaseException dbQueryAllFail) {
        this.dbQueryAllFail.setValue(dbQueryAllFail);
    }

    public SingleLiveEvent<List<RandomUser>> getDbQueryAllSuccess() {
        return dbQueryAllSuccess;
    }

    private void onDbQueryAllSuccess(List<RandomUser> dbQueryAllSuccess) {
        this.dbQueryAllSuccess.setValue(dbQueryAllSuccess);
    }

}
