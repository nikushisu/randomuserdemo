package com.nibalk.randomuser.app.di;

import android.arch.lifecycle.ViewModelProvider;

import com.nibalk.framework.base.viewmodel.ViewModelProviderFactory;
import com.nibalk.randomuser.app.data.repository.RandomUserRepository;
import com.nibalk.randomuser.app.view.fragment.RandomUserQueryFragment;
import com.nibalk.randomuser.app.viewmodel.RandomUserActivityViewModel;
import com.nibalk.randomuser.app.viewmodel.RandomUserQueryFragmentViewModel;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

@Module
public class RandomUserQueryFragmentModule {

    @Module
    public static abstract class RandomUserQueryFragmentProvider {
        @ContributesAndroidInjector(modules = RandomUserQueryFragmentModule.class)
        abstract RandomUserQueryFragment provideRandomUserQueryFragment();
    }

    @Provides
    RandomUserQueryFragmentViewModel randomUserQueryFragmentViewModel(RandomUserRepository repository) {
        return new RandomUserQueryFragmentViewModel(repository);
    }

    @Provides
    @Named("FRAGMENT")
    ViewModelProvider.Factory provideRandomUserQueryFragmentViewModel(RandomUserQueryFragmentViewModel viewModel) {
        return new ViewModelProviderFactory<>(viewModel);
    }
}
