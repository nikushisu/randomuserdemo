package com.nibalk.randomuser.app.viewmodel;

import android.text.TextUtils;

import com.nibalk.framework.base.DateUtils;
import com.nibalk.framework.base.viewmodel.BaseViewModel;
import com.nibalk.framework.storage.roomdb.manager.RoomDbManager;
import com.nibalk.randomuser.app.data.model.RandomUser;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class RandomUserFragmentBaseViewModel extends BaseViewModel {

    public String getFormattedDob(String dob) {
        String dobSubStr = dob.substring(0, Math.min(dob.length(), 10));

        return DateUtils.convertOneDateFormatToAnother(dobSubStr,
                "yyyy-MM-dd", "dd MMM YYYY", Locale.ENGLISH);
    }

    public String getUserFullName(RandomUser.Name userName) {
        String fullName = "";
        if (userName != null) {
            String firstName = (TextUtils.isEmpty(userName.getFirst())) ? "" : userName.getFirst();
            String lastName = (TextUtils.isEmpty(userName.getLast())) ? "" : userName.getLast();

            fullName = firstName + " " + lastName;
        }
        return fullName;
    }

    public RandomUser encryptRandomUser(RoomDbManager roomDbManager, RandomUser unencryptedUser) {

        RandomUser encryptedUser = unencryptedUser;

        if (unencryptedUser != null) {
            encryptedUser.setEmail(roomDbManager.encryptedValue(unencryptedUser.getEmail()));
            encryptedUser.setPhone(roomDbManager.encryptedValue(unencryptedUser.getPhone()));

            RandomUser.Name encryptedName = new RandomUser.Name();
            encryptedName.setFirst(roomDbManager.encryptedValue(unencryptedUser.getName().getFirst()));
            encryptedName.setLast(roomDbManager.encryptedValue(unencryptedUser.getName().getLast()));
            encryptedUser.setName(encryptedName);
        }

        return encryptedUser;
    }

    public List<RandomUser> decryptRandomUserList(RoomDbManager roomDbManager,
                                                  List<RandomUser> encryptedUserList) {

        List<RandomUser> decryptedUserList = new ArrayList<>();

        for (RandomUser encryptedUser: encryptedUserList) {
            RandomUser decryptedUser = encryptedUser;

            if (encryptedUser != null) {
                decryptedUser.setEmail(roomDbManager.decryptedValue(encryptedUser.getEmail()));
                decryptedUser.setPhone(roomDbManager.decryptedValue(encryptedUser.getPhone()));

                RandomUser.Name decryptedName = new RandomUser.Name();
                decryptedName.setFirst(roomDbManager.decryptedValue(encryptedUser.getName().getFirst()));
                decryptedName.setLast(roomDbManager.decryptedValue(encryptedUser.getName().getLast()));
                decryptedUser.setName(decryptedName);
            }

            decryptedUserList.add(decryptedUser);
        }

        return decryptedUserList;
    }


}
