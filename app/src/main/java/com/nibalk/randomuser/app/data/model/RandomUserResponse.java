package com.nibalk.randomuser.app.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RandomUserResponse {

    @Expose
    @SerializedName("info")
    private RandomUserInfo info;
    @Expose
    @SerializedName("results")
    private List<RandomUser> randomUsers;

    public RandomUserResponse() {}

    public RandomUserInfo getRandomUserInfo() {
        return info;
    }

    public void setRandomUserInfo(RandomUserInfo info) {
        this.info = info;
    }

    public List<RandomUser> getRandomUsers() {
        return randomUsers;
    }

    public void setRandomUsers(List<RandomUser> randomUsers) {
        this.randomUsers = randomUsers;
    }
}
