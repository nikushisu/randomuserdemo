package com.nibalk.randomuser.app.di;

import android.arch.lifecycle.ViewModelProvider;

import com.nibalk.framework.base.di.annotation.ActivityScope;
import com.nibalk.framework.base.viewmodel.ViewModelProviderFactory;
import com.nibalk.randomuser.app.view.NavigationController;
import com.nibalk.randomuser.app.view.activity.MainActivity;
import com.nibalk.randomuser.app.viewmodel.MainActivityViewModel;

import dagger.Module;
import dagger.Provides;

@Module
public class MainActivityModule {

    @Provides
    MainActivityViewModel mainActivityViewModel() {
        return new MainActivityViewModel();
    }

    @Provides
    ViewModelProvider.Factory provideMainActivityViewModel(MainActivityViewModel viewModel) {
        return new ViewModelProviderFactory<>(viewModel);
    }

    @Provides
    @ActivityScope
    public NavigationController provideNavigationController(MainActivity activity) {
        return new NavigationController(activity);
    }

}
