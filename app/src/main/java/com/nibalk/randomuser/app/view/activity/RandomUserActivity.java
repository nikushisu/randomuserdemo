package com.nibalk.randomuser.app.view.activity;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.nibalk.framework.base.view.BaseActivity;
import com.nibalk.randomuser.R;
import com.nibalk.randomuser.app.view.NavigationController;
import com.nibalk.randomuser.app.viewmodel.RandomUserActivityViewModel;
import com.nibalk.randomuser.databinding.ActivityRandomUserBinding;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import timber.log.Timber;

import static com.nibalk.randomuser.app.utils.Constants.Keys.BUNDLE_DATA_FROM_SERVER;

public class RandomUserActivity extends BaseActivity<RandomUserActivityViewModel, ActivityRandomUserBinding>
        implements NavigationView.OnNavigationItemSelectedListener, HasSupportFragmentInjector {

    private static final String TAG = "RandomUserActivity";

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingFragmentInjector;

    @Inject
    NavigationController navigationController;

    private ActivityRandomUserBinding binding;
    private RandomUserActivityViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_random_user);

        initScreen();
        selectItem(0);
    }

    @Override
    protected RandomUserActivityViewModel setupViewModel() {
        Timber.d( "setupViewModel");
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(RandomUserActivityViewModel.class);
        return viewModel;
    }

    @Override
    protected ActivityRandomUserBinding setupDataBinding() {
        Timber.d("setupDataBinding");
        binding = DataBindingUtil.setContentView(this, R.layout.activity_random_user);
        return binding;
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingFragmentInjector;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_query_user) {
            selectItem(0);
        } else if (id == R.id.nav_view_user) {
            selectItem(1);
        } else if (id == R.id.nav_exit) {
            selectItem(2);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void selectItem(int position) {

        switch (position) {
            case 0:
                navigationController.loadRandomUserQueryFragment(null);
                break;
            case 1:
                Bundle bundle = new Bundle();
                bundle.putBoolean(BUNDLE_DATA_FROM_SERVER, false);
                navigationController.loadRandomUserListFragment(null);
                break;
            case 2:
                finish();
                break;
            default:
                break;
        }
    }

    private void initScreen() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }
}
