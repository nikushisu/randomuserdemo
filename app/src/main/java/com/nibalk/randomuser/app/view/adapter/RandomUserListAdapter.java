package com.nibalk.randomuser.app.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nibalk.randomuser.R;
import com.nibalk.randomuser.app.data.model.RandomUser;
import com.nibalk.randomuser.app.view.utils.RandomUserListItemClickListener;
import com.nibalk.randomuser.databinding.LayoutCardviewBinding;

import java.util.List;


public class RandomUserListAdapter extends RecyclerView.Adapter<RandomUserListAdapter.RandomUserViewHolder> {

    private RandomUserListItemClickListener listener;
    private List<RandomUser> randomUserList;

    public RandomUserListAdapter(RandomUserListItemClickListener listener, List<RandomUser> randomUserList) {
        this.listener = listener;
        this.randomUserList = randomUserList;
    }

    @NonNull
    @Override
    public RandomUserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutCardviewBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.layout_cardview, parent, false);

        return new RandomUserViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RandomUserViewHolder holder, int position) {
        RandomUser user = randomUserList.get(position);
        holder.binding.setModel(user);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemCount() {
        return randomUserList.size();
    }


    class RandomUserViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        LayoutCardviewBinding binding;

        RandomUserViewHolder(LayoutCardviewBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
            binding.randomUserCardView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onClicked(getAdapterPosition());
        }
    }
}