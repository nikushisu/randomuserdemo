package com.nibalk.randomuser.app.di;

import android.app.Application;

import com.nibalk.framework.base.di.FrameworkModule;
import com.nibalk.randomuser.app.RandomUserApp;
import com.nibalk.randomuser.app.di.module.ActivityBindingModule;
import com.nibalk.randomuser.app.di.module.AppModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {AndroidSupportInjectionModule.class, FrameworkModule.class, AppModule.class, ActivityBindingModule.class})
public interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application app);

        AppComponent build();
    }

    void inject(RandomUserApp app);
}
