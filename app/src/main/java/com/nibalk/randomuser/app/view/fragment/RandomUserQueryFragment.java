package com.nibalk.randomuser.app.view.fragment;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.nibalk.framework.base.view.BaseFragment;
import com.nibalk.randomuser.R;
import com.nibalk.randomuser.app.data.model.RandomUser;
import com.nibalk.randomuser.app.view.NavigationController;
import com.nibalk.randomuser.app.view.activity.MainActivity;
import com.nibalk.randomuser.app.viewmodel.RandomUserQueryFragmentViewModel;
import com.nibalk.randomuser.databinding.FragmentRandomUserQueryBinding;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import timber.log.Timber;

import static com.nibalk.randomuser.app.utils.Constants.Keys.BUNDLE_DATA_FROM_SERVER;
import static com.nibalk.randomuser.app.utils.Constants.Keys.BUNDLE_USER_DATA;

public class RandomUserQueryFragment extends BaseFragment<RandomUserQueryFragmentViewModel, FragmentRandomUserQueryBinding> {

    @Named("FRAGMENT")
    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Inject
    NavigationController navigationController;


    private RandomUserQueryFragmentViewModel viewModel;
    private FragmentRandomUserQueryBinding binding;

    private Context context;
    private String selectedGender = "";

    public static RandomUserQueryFragment newInstance() {
        return new RandomUserQueryFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_random_user_query, container, false);
        binding.setViewModel(setupViewModel());
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.context = getActivity();

        initScreen();
    }

    @Override
    protected RandomUserQueryFragmentViewModel setupViewModel() {
        Timber.d( "setupViewModel()");
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(RandomUserQueryFragmentViewModel.class);
        return viewModel;
    }

    @Override
    protected FragmentRandomUserQueryBinding setupDataBinding() {
        Timber.d("setupDataBinding()");
        return binding;
    }

    private void onReceivedDataOrError() {
        viewModel.getQueryUserSuccess().observe(getActivity(), user -> {
            Timber.d("onReceivedData");

            setProgressBarVisibility(binding.progressBarHolder,
                    1f, 0f, 200, View.INVISIBLE);

            if (user != null) {
                Timber.d("onReceivedData -> %s", user.toString());
                loadViewUserForm(user);
            }
        });

        viewModel.getQueryUsersSuccess().observe(getActivity(), users -> {
            Timber.d("onReceivedData");

            setProgressBarVisibility(binding.progressBarHolder,
                    1f, 0f, 200, View.INVISIBLE);

            if (users != null) {
                Timber.d("onReceivedData -> %s", users.toString());
                loadViewUserList(users);
            } else {
                Toast.makeText(context, "Error - No users to display", Toast.LENGTH_LONG).show();
            }
        });

        viewModel.getQueryUserFail().observe(getActivity(), error -> {
            Timber.d("onReceivedError");

            String message = " - ";

            if (error != null) {
                Timber.d("onReceivedError -> %s", error.getMessage());
                message = message + error.getErrorMessage();
            }
            Toast.makeText(context, "Error" + message, Toast.LENGTH_LONG).show();

            setProgressBarVisibility(binding.progressBarHolder,
                    1f, 0f, 200, View.GONE);

            showAlertDialog("Do you want to retry ?");
        });
    }

    private void loadViewUserList(List<RandomUser> users) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(BUNDLE_USER_DATA, new ArrayList<RandomUser>(users));
        bundle.putBoolean(BUNDLE_DATA_FROM_SERVER, true);
        navigationController.popBackStack();
        navigationController.loadRandomUserListFragment(bundle);
    }

    private void loadViewUserForm(RandomUser user) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(BUNDLE_USER_DATA, user);
        bundle.putBoolean(BUNDLE_DATA_FROM_SERVER, true);
        navigationController.popBackStack();
        navigationController.loadRandomUserViewFragment(bundle);
    }

    private void queryData() {
        Timber.d("queryData()");

        onReceivedDataOrError();

        String userId = binding.editTextUserId.getText().toString();
        String count = binding.editTextCount.getText().toString();

        setProgressBarVisibility(binding.progressBarHolder,
                0f, 1f, 200, View.VISIBLE);

        if (!TextUtils.isEmpty(count)) {
            viewModel.getRandomUsersByCount(count);
        } else if (!TextUtils.isEmpty(userId)) {
            viewModel.getRandomUserByUserId(userId);
        } else if (!TextUtils.isEmpty(selectedGender)) {
            viewModel.getRandomUserByGender(selectedGender.toLowerCase());
        } else {
            setProgressBarVisibility(binding.progressBarHolder,
                    1f, 0f, 200, View.INVISIBLE);

            Toast.makeText(context, "Please fill one field above.", Toast.LENGTH_LONG).show();
        }
    }

    private void initScreen() {
        Timber.d("initScreen()");

        binding.buttonQuery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                queryData();
            }
        });

        AutoCompleteTextView spinnerGender = binding.autoCompleteTextGender;

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                context, android.R.layout.simple_list_item_1, getResources()
                .getStringArray(R.array.gender_label));

        spinnerGender.setAdapter(arrayAdapter);
        spinnerGender.setCursorVisible(false);

        spinnerGender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View arg0) {
                spinnerGender.showDropDown();
            }
        });

        spinnerGender.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                spinnerGender.showDropDown();
                selectedGender = (String) parent.getItemAtPosition(position);
                Timber.d( "SelectedGender = %s", selectedGender);
            }
        });
    }

    private void showAlertDialog(String errorMessage) {
        Timber.d("showAlertDialog()");

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setMessage(errorMessage)
                .setCancelable(false)
                .setPositiveButton("Retry", (dialog, id) -> {
                    dialog.cancel();
                    Timber.d("showAlertDialog() - Click retry");
                    queryData();
                })
                .setNegativeButton("Cancel", (dialog, id) -> {
                    dialog.cancel();
                    Timber.d("showAlertDialog() - Click cancel");
                });

        AlertDialog alert = builder.create();
        alert.setTitle("Network Error");
        alert.show();
    }
}
