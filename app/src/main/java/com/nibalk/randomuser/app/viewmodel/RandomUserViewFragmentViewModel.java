package com.nibalk.randomuser.app.viewmodel;

import com.nibalk.framework.base.error.BaseException;
import com.nibalk.framework.base.viewmodel.SingleLiveEvent;
import com.nibalk.framework.storage.roomdb.manager.RoomDbManager;
import com.nibalk.randomuser.app.data.model.RandomUser;
import com.nibalk.randomuser.app.data.repository.RandomUserRepository;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class RandomUserViewFragmentViewModel extends RandomUserFragmentBaseViewModel {

    private RandomUserRepository randomUserRepository;
    private RoomDbManager roomDbManager;

    private SingleLiveEvent<Boolean> onStoreDataClicked = new SingleLiveEvent<>();
    private SingleLiveEvent<Boolean> onDeleteDataClicked = new SingleLiveEvent<>();
    private SingleLiveEvent<BaseException> dbInsertFail = new SingleLiveEvent<>();
    private SingleLiveEvent<Long> dbInsertSuccess = new SingleLiveEvent<>();
    private SingleLiveEvent<BaseException> dbDeleteFail = new SingleLiveEvent<>();
    private SingleLiveEvent<Integer> dbDeleteSuccess = new SingleLiveEvent<>();


    public RandomUserViewFragmentViewModel(RandomUserRepository randomUserRepository,
                                           RoomDbManager roomDbManager) {
        this.randomUserRepository = randomUserRepository;
        this.roomDbManager = roomDbManager;
    }

    public void storeRandomUser(RandomUser user) {
        Timber.d("storeRandomUser()");

        onStoreDataClicked(true);

        user = encryptRandomUser(roomDbManager, user);

        Disposable disposable = randomUserRepository.insertRandomUser(user)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    Timber.d("storeRandomUser - response %s", response);
                    onDbInsertSuccess(response);
                }, throwable -> {
                    Timber.d("storeRandomUser - error");
                    onDbInsertFail(new BaseException(throwable));
                });
        getCompositeDisposable().add(disposable);
    }

    public void removeRandomUser(RandomUser user) {
        Timber.d("removeRandomUser()");

        onDeleteDataClicked(true);

        Disposable disposable = randomUserRepository.deleteRandomUser(user.getUserId())
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    Timber.d("removeRandomUser - response %s", response);
                    onDbDeleteSuccess(response);
                }, throwable -> {
                    Timber.d("removeRandomUser - error");
                    onDbDeleteFail(new BaseException(throwable));
                });
        getCompositeDisposable().add(disposable);

    }

    public SingleLiveEvent<Boolean> getOnStoreDataClicked() {
        return onStoreDataClicked;
    }

    private void onStoreDataClicked(Boolean onStoreDataClicked) {
        this.onStoreDataClicked.setValue(onStoreDataClicked);
    }

    public SingleLiveEvent<Boolean> getOnDeleteDataClicked() {
        return onDeleteDataClicked;
    }

    private void onDeleteDataClicked(Boolean onDeleteDataClicked) {
        this.onDeleteDataClicked.setValue(onDeleteDataClicked);
    }

    public SingleLiveEvent<BaseException> getDbInsertFail() {
        return dbInsertFail;
    }

    private void onDbInsertFail(BaseException dbInsertFail) {
        this.dbInsertFail.setValue(dbInsertFail);
    }

    public SingleLiveEvent<Long> getDbInsertSuccess() {
        return dbInsertSuccess;
    }

    private void onDbInsertSuccess(Long dbInsertSuccess) {
        this.dbInsertSuccess.setValue(dbInsertSuccess);
    }

    //
    public SingleLiveEvent<BaseException> getDbDeleteFail() {
        return dbDeleteFail;
    }

    private void onDbDeleteFail(BaseException dbDeleteFail) {
        this.dbDeleteFail.setValue(dbDeleteFail);
    }

    public SingleLiveEvent<Integer> getDbDeleteSuccess() {
        return dbDeleteSuccess;
    }

    private void onDbDeleteSuccess(Integer dbDeleteSuccess) {
        this.dbDeleteSuccess.setValue(dbDeleteSuccess);
    }
}
