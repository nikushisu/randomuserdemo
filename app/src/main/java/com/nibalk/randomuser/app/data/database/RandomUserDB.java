package com.nibalk.randomuser.app.data.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.nibalk.randomuser.app.data.model.RandomUser;

import timber.log.Timber;

import static android.support.constraint.Constraints.TAG;
import static com.nibalk.randomuser.app.utils.Constants.Values.DB_NAME;

@Database(entities = {RandomUser.class}, version = 1, exportSchema = false)
public abstract class RandomUserDB extends RoomDatabase {

    public abstract RandomUserDao getRandomUserDao();

    private static RandomUserDB instance;

    public static RandomUserDB getRoomDbManager(Context context) {
        if (instance == null) {
            Timber.d("RoomDB initialization");

            final Migration migration = new Migration(0, 1) {
                @Override
                public void migrate(@NonNull SupportSQLiteDatabase database) {
                    Log.d(TAG,"RoomDB migration");
                }
            };

            instance = Room.databaseBuilder(context.getApplicationContext(),
                    RandomUserDB.class, DB_NAME)
                    .addMigrations(migration)
                    .build();
        }
        return instance;
    }
}
