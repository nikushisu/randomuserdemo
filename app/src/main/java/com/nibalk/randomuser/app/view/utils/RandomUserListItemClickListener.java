package com.nibalk.randomuser.app.view.utils;

public interface RandomUserListItemClickListener {

    void onClicked(int position);
}
