package com.nibalk.randomuser.app.data.repository;

import android.content.Context;

import com.nibalk.framework.network.manager.NetworkManager;
import com.nibalk.randomuser.app.data.database.RandomUserDB;
import com.nibalk.randomuser.app.data.model.RandomUser;
import com.nibalk.randomuser.app.data.model.RandomUserResponse;
import com.nibalk.randomuser.app.data.service.RandomUserService;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import timber.log.Timber;

public class RandomUserRepositoryImpl implements RandomUserRepository {

    private Context context;
    private RandomUserDB randomUserDB;
    private RandomUserService randomUserService;
    private NetworkManager networkManager;

    public RandomUserRepositoryImpl(Context context, RandomUserDB randomUserDB,
                                    RandomUserService randomUserService, NetworkManager networkManager) {
        this.context = context;
        this.randomUserDB = randomUserDB;
        this.randomUserService = randomUserService;
        this.networkManager = networkManager;
    }

    @Override
    public Observable<RandomUserResponse> getRandomUserByGender(String gender) {
        Timber.d( "getRandomUserByGender()");

        Call<RandomUserResponse> call = randomUserService.fetchUserByGender(gender);
        return getRandomUserData(call);
    }

    @Override
    public Observable<RandomUserResponse> getRandomUserBySeed(String seed) {
        Timber.d( "getRandomUserBySeed()");

        Call<RandomUserResponse> call = randomUserService.fetchUserBySeed(seed);
        return getRandomUserData(call);
    }

    @Override
    public Observable<RandomUserResponse> getRandomUsers(String count) {
        Timber.d( "getQueryUsersSuccess()");

        Call<RandomUserResponse> call = randomUserService.fetchUsers(count);
        return getRandomUserData(call);
    }

    @Override
    public Observable<Long> insertRandomUser(RandomUser user) {
        Timber.d( "insertRandomUser()");

        return Observable.fromCallable(() -> {
            return randomUserDB.getRandomUserDao().insertUser(user);
        });
    }

    @Override
    public Observable<List<RandomUser>> selectAllRandomUsers() {
        Timber.d( "selectAllRandomUsers()");

        return Observable.fromCallable(() -> {
            return randomUserDB.getRandomUserDao().selectAllUsers();
        });
    }

    @Override
    public Observable<Integer> deleteRandomUser(int userId) {
        Timber.d( "deleteRandomUser()");

        return Observable.fromCallable(() -> {
            return randomUserDB.getRandomUserDao().deleteUser(userId);
        });
    }

    private Observable<RandomUserResponse> getRandomUserData(Call<RandomUserResponse> call) {
        return networkManager.execute(call)
                .map(response -> {
                    if (response != null) {
                        Timber.d("Fetched users count = " + response.getRandomUsers().size());
                        Timber.d( response.toString());
                    } else {
                        Timber.d("Fetched response is null");
                    }
                    return response;
                });
    }
}
