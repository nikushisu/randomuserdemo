package com.nibalk.randomuser.app.di;

import android.arch.lifecycle.ViewModelProvider;

import com.nibalk.framework.base.viewmodel.ViewModelProviderFactory;
import com.nibalk.framework.storage.roomdb.manager.RoomDbManager;
import com.nibalk.randomuser.app.data.repository.RandomUserRepository;
import com.nibalk.randomuser.app.view.activity.RandomUserActivity;
import com.nibalk.randomuser.app.view.fragment.RandomUserListFragment;
import com.nibalk.randomuser.app.viewmodel.RandomUserActivityViewModel;
import com.nibalk.randomuser.app.viewmodel.RandomUserListFragmentViewModel;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

@Module
public class RandomUserListFragmentModule {

    @Module
    public static abstract class RandomUserListFragmentProvider {
        @ContributesAndroidInjector(modules = RandomUserListFragmentModule.class)
        abstract RandomUserListFragment provideRandomUserListFragment();
    }

    @Provides
    RandomUserListFragmentViewModel randomUserListFragmentViewModel(RandomUserRepository repository,
                                                                    RoomDbManager roomDbManager) {
        return new RandomUserListFragmentViewModel(repository, roomDbManager);
    }

    @Provides
    @Named("FRAGMENT")
    ViewModelProvider.Factory provideRandomUserListFragmentViewModel(RandomUserListFragmentViewModel viewModel) {
        return new ViewModelProviderFactory<>(viewModel);
    }
}
