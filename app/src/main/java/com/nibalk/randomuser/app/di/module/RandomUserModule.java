package com.nibalk.randomuser.app.di.module;

import android.content.Context;

import com.nibalk.framework.network.manager.NetworkManager;
import com.nibalk.randomuser.app.data.database.RandomUserDB;
import com.nibalk.randomuser.app.data.repository.RandomUserRepository;
import com.nibalk.randomuser.app.data.repository.RandomUserRepositoryImpl;
import com.nibalk.randomuser.app.data.service.RandomUserService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class RandomUserModule {

    @Provides
    @Singleton
    RandomUserRepository provideRandomUserRepository(Context context,
                                                     RandomUserDB randomUserDb,
                                                     RandomUserService randomUserService,
                                                     NetworkManager networkManager) {
        return new RandomUserRepositoryImpl(context, randomUserDb, randomUserService, networkManager);
    }

    @Provides
    @Singleton
    RandomUserService provideRandomUserService(Retrofit retrofit) {
        return retrofit.create(RandomUserService.class);
    }

    @Provides
    @Singleton
    RandomUserDB provideRandomUserDB(Context context) {
        return RandomUserDB.getRoomDbManager(context);
    }
}
