package com.nibalk.randomuser.app.data.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.nibalk.randomuser.app.data.model.RandomUser;

import java.util.List;

import static com.nibalk.randomuser.app.utils.Constants.Values.DB_TABLE_NAME;

@Dao
public interface RandomUserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insertUser(RandomUser user);

    @Query("SELECT * FROM " + DB_TABLE_NAME)
    List<RandomUser> selectAllUsers();

    @Query("DELETE FROM " + DB_TABLE_NAME + " WHERE user_id = :userId ")
    int deleteUser(int userId);

}
