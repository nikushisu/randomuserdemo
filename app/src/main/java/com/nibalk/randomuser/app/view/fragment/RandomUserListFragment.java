package com.nibalk.randomuser.app.view.fragment;


import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.nibalk.framework.base.view.BaseFragment;
import com.nibalk.framework.security.encryption.manager.EncryptionManager;
import com.nibalk.randomuser.R;
import com.nibalk.randomuser.app.data.model.RandomUser;
import com.nibalk.randomuser.app.view.NavigationController;
import com.nibalk.randomuser.app.view.adapter.RandomUserListAdapter;
import com.nibalk.randomuser.app.view.utils.RandomUserListItemClickListener;
import com.nibalk.randomuser.app.viewmodel.RandomUserListFragmentViewModel;
import com.nibalk.randomuser.databinding.FragmentRandomUserListBinding;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import timber.log.Timber;

import static com.nibalk.framework.base.Constants.Security.ENCRYPTION_MANAGER_WITH_KEY;
import static com.nibalk.randomuser.app.utils.Constants.Keys.BUNDLE_DATA_FROM_SERVER;
import static com.nibalk.randomuser.app.utils.Constants.Keys.BUNDLE_USER_DATA;

public class RandomUserListFragment extends BaseFragment<RandomUserListFragmentViewModel, FragmentRandomUserListBinding> {

    @Named("FRAGMENT")
    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Inject
    NavigationController navigationController;


    private RandomUserListFragmentViewModel viewModel;
    private FragmentRandomUserListBinding binding;

    private Context context;
    private boolean isDataFromServer = false;
    private List<RandomUser> list = new ArrayList<>();
    private RandomUserListAdapter adapter;



    public static RandomUserListFragment newInstance() {
        return new RandomUserListFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_random_user_list, container, false);
        binding.setViewModel(setupViewModel());
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.context = getActivity();

        initScreen();
    }

    @Override
    protected RandomUserListFragmentViewModel setupViewModel() {
        Timber.d("setupViewModel()");
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(RandomUserListFragmentViewModel.class);
        return viewModel;
    }

    @Override
    protected FragmentRandomUserListBinding setupDataBinding() {
        Timber.d("setupDataBinding()");
        return binding;
    }

    private void initScreen() {
        list = getRandomUsersToDisplay();

        adapter = new RandomUserListAdapter(listItemClickListener, list);

        binding.list.setLayoutManager(new LinearLayoutManager(context));
        binding.list.setAdapter(adapter);

        showProgressBar();

        if (isDataFromServer) {
            hideProgressBar();
            if (list.isEmpty()) {
                binding.tvNoData.setVisibility(View.VISIBLE);
            }
        } else {
            onQuerySuccessOrError();
            viewModel.queryStoredRandomUser();
        }
    }

    private List<RandomUser> getRandomUsersToDisplay() {
        Timber.d("getRandomUserToDisplay()");

        ArrayList<RandomUser> randomUsers = new ArrayList<>();
        Bundle bundle = getArguments();
        if (bundle != null) {
            randomUsers = bundle.getParcelableArrayList(BUNDLE_USER_DATA);
            isDataFromServer = bundle.getBoolean(BUNDLE_DATA_FROM_SERVER);
            if (randomUsers != null) {
                Timber.d("getRandomUserToDisplay() - users -> %s", randomUsers.toString());
            }
        }
        return randomUsers;
    }

    private void onQuerySuccessOrError() {

        viewModel.getDbQueryAllSuccess().observe(getActivity(), userList -> {
            Timber.d("getDbQueryAllSuccess");

            hideProgressBar();

            if (userList != null && !userList.isEmpty()) {
                Timber.d("getDbQueryAllSuccess -> %s", userList.toString());

                list.addAll(userList);
                adapter.notifyDataSetChanged();
            } else {
                binding.tvNoData.setVisibility(View.VISIBLE);
            }

        });

        viewModel.getDbQueryAllFail().observe(getActivity(), error -> {
            Timber.d("getDbQueryAllFail");

            hideProgressBar();

            String message = " - ";
            if (error != null) {
                Timber.d("onReceivedError -> %s", error.getMessage());
                message = message + error.getErrorMessage();
            }
            Toast.makeText(context, "Error" + message, Toast.LENGTH_LONG).show();
        });
    }

    private void loadViewUserForm(RandomUser user) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(BUNDLE_USER_DATA, user);
        bundle.putBoolean(BUNDLE_DATA_FROM_SERVER, isDataFromServer);
        navigationController.popBackStack();
        navigationController.loadRandomUserViewFragment(bundle);
    }

    private RandomUserListItemClickListener listItemClickListener = new RandomUserListItemClickListener() {
        @Override
        public void onClicked(int position) {
            Timber.d("listItem clicked -> %d", position);
            loadViewUserForm(list.get(position));
        }

    };

    private void hideProgressBar() {
        setProgressBarVisibility(binding.progressBarHolder,
                1f, 0f, 200, View.GONE);
    }

    private void showProgressBar() {
        setProgressBarVisibility(binding.progressBarHolder,
                0f, 1f, 200, View.VISIBLE);
    }
}
