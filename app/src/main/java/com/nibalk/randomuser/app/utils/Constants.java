package com.nibalk.randomuser.app.utils;

public class Constants {

    public static class AppUrls {
        public static final String BASE_URL = "https://randomuser.me/api/";
    }

    public static class Keys {
        public final static String QUERY_PARAMS_GENDER_LABEL = "gender";
        public final static String QUERY_PARAMS_SEED_LABEL = "seed";
        public final static String QUERY_PARAMS_RESULTS_LABEL = "results";
        public final static String BUNDLE_USER_DATA = "BUNDLE_USER_DATA";
        public final static String BUNDLE_DATA_FROM_SERVER = "BUNDLE_DATA_FROM_SERVER";
    }

    public static class Values {
        public static final int DB_VERSION = 1;
        public static final String DB_NAME = "RandoUserDB.db";
        public static final String DB_TABLE_NAME = "random_user";
    }
}
