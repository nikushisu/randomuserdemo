package com.nibalk.randomuser.app.view.fragment;


import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.nibalk.framework.base.view.BaseFragment;
import com.nibalk.randomuser.R;
import com.nibalk.randomuser.app.data.model.RandomUser;
import com.nibalk.randomuser.app.view.NavigationController;
import com.nibalk.randomuser.app.viewmodel.RandomUserViewFragmentViewModel;
import com.nibalk.randomuser.databinding.FragmentRandomUserViewBinding;

import javax.inject.Inject;
import javax.inject.Named;

import timber.log.Timber;

import static com.nibalk.randomuser.app.utils.Constants.Keys.BUNDLE_DATA_FROM_SERVER;
import static com.nibalk.randomuser.app.utils.Constants.Keys.BUNDLE_USER_DATA;

public class RandomUserViewFragment extends BaseFragment<RandomUserViewFragmentViewModel, FragmentRandomUserViewBinding> {

    @Named("FRAGMENT")
    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Inject
    NavigationController navigationController;

    private RandomUserViewFragmentViewModel viewModel;
    private FragmentRandomUserViewBinding binding;

    private Context context;
    private boolean isDataFromServer = false;

    public static RandomUserViewFragment newInstance() {
        return new RandomUserViewFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_random_user_view, container, false);
        binding.setViewModel(setupViewModel());
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.context = getActivity();

        initScreen();
    }

    @Override
    protected RandomUserViewFragmentViewModel setupViewModel() {
        Timber.d("setupViewModel()");
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(RandomUserViewFragmentViewModel.class);
        return viewModel;
    }

    @Override
    protected FragmentRandomUserViewBinding setupDataBinding() {
        Timber.d("setupDataBinding()");
        return binding;
    }

    private void initScreen() {
        Timber.d("initScreen()");

        displayRandomUser();
        setButtonVisibility();

        viewModel.getOnStoreDataClicked().observe(getActivity(), isClicked -> {
            Timber.d("getOnStoreDataClicked");

            if (isClicked != null && isClicked) {
                Timber.d("getOnStoreDataClicked - storing data");

                showProgressBar();
                onInsertSuccessOrError();
            }
        });

        viewModel.getOnDeleteDataClicked().observe(getActivity(), isClicked -> {
            Timber.d("getOnDeleteDataClicked");

            if (isClicked != null && isClicked) {
                Timber.d("getOnDeleteDataClicked - deleting data");

                showProgressBar();
                onDeleteSuccessOrError();
            }
        });
    }

    private void setButtonVisibility() {
        binding.buttonStore.setVisibility(isDataFromServer ? View.VISIBLE : View.GONE);
        binding.buttonDelete.setVisibility(!isDataFromServer ? View.VISIBLE : View.GONE);

    }

    private void displayRandomUser() {
        Timber.d("displayRandomUser()");

        RandomUser user = getRandomUserToDisplay();

        if (user != null) {
            Timber.d("displayRandomUser() - user -> %s", user.toString());

            binding.setRandomUser(user);
        } else {
            Toast.makeText(context, "No user data to be shown", Toast.LENGTH_LONG).show();
        }
    }

    private RandomUser getRandomUserToDisplay() {
        Timber.d("getRandomUserToDisplay()");

        RandomUser randomUser = null;
        Bundle bundle = getArguments();
        if (bundle != null) {
            randomUser = bundle.getParcelable(BUNDLE_USER_DATA);
            isDataFromServer = bundle.getBoolean(BUNDLE_DATA_FROM_SERVER);
            if (randomUser != null) {
                Timber.d("getRandomUserToDisplay() - user -> %s", randomUser.toString());
            }
        }
        return randomUser;
    }

    private void onInsertSuccessOrError() {
        Timber.d("onInsertSuccessOrError()");

        viewModel.getDbInsertSuccess().observe(getActivity(), response -> {
            Timber.d("onDbInsertSuccess -> %s", response);

            Toast.makeText(context, "User is successfully stored in the database.", Toast.LENGTH_LONG).show();
            hideProgressBar();
        });

        viewModel.getDbInsertFail().observe(getActivity(), error -> {
            Timber.d("onDbInsertFail");

            String message = " - ";

            if (error != null) {
                Timber.d("onReceivedError -> %s", error.getMessage());
                message = message + error.getErrorMessage();
            }
            Toast.makeText(context, "Error" + message, Toast.LENGTH_LONG).show();
            hideProgressBar();
        });
    }

    private void onDeleteSuccessOrError() {
        Timber.d("onDeleteSuccessOrError()");

        viewModel.getDbDeleteSuccess().observe(getActivity(), response -> {
            Timber.d("onDbDeleteSuccess -> %s", response);

            Toast.makeText(context, "User is successfully deleted from the database.", Toast.LENGTH_LONG).show();
            hideProgressBar();

            Bundle bundle = new Bundle();
            bundle.putBoolean(BUNDLE_DATA_FROM_SERVER, false);
            navigationController.loadRandomUserListFragment(null);
        });

        viewModel.getDbInsertFail().observe(getActivity(), error -> {
            Timber.d("onDbDeleteFail");

            String message = " - ";

            if (error != null) {
                Timber.d("onReceivedError -> %s", error.getMessage());
                message = message + error.getErrorMessage();
            }
            Toast.makeText(context, "Error" + message, Toast.LENGTH_LONG).show();
            hideProgressBar();
        });
    }

    private void hideProgressBar() {
        setProgressBarVisibility(binding.progressBarHolder,
                1f, 0f, 200, View.GONE);
    }

    private void showProgressBar() {
        setProgressBarVisibility(binding.progressBarHolder,
                0f, 1f, 200, View.VISIBLE);
    }
}

