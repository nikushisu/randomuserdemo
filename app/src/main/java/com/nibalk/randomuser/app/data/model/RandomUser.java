package com.nibalk.randomuser.app.data.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "random_user")
public class RandomUser implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "user_id")
    private int userId;

    @ColumnInfo(name = "user_seed")
    private String seed;

    @Expose
    @SerializedName("name")
    @Embedded
    private Name name;

    @Expose
    @SerializedName("dob")
    @Embedded
    private Dob dob;

    @Expose
    @SerializedName("phone")
    @ColumnInfo(name = "phone_no")
    private String phone;

    @Expose
    @SerializedName("email")
    @ColumnInfo(name = "email_addr")
    private String email;

    @Expose
    @SerializedName("gender")
    @ColumnInfo(name = "user_gender")
    private String gender;

    public RandomUser() {}

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getSeed() {
        return seed;
    }

    public void setSeed(String seed) {
        this.seed = seed;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public Dob getDob() {
        return dob;
    }

    public void setDob(Dob dob) {
        this.dob = dob;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public static class Dob implements Parcelable {
        @Expose
        @SerializedName("age")
        @ColumnInfo(name = "user_age")
        private int age;

        @Expose
        @SerializedName("date")
        @ColumnInfo(name = "user_dob")
        private String date;

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        @Override
        public String toString() {
            return "Dob{" +
                    "age=" + age +
                    ", date='" + date + '\'' +
                    '}';
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.age);
            dest.writeString(this.date);
        }

        public Dob() {
        }

        protected Dob(Parcel in) {
            this.age = in.readInt();
            this.date = in.readString();
        }

        public static final Parcelable.Creator<Dob> CREATOR = new Parcelable.Creator<Dob>() {
            @Override
            public Dob createFromParcel(Parcel source) {
                return new Dob(source);
            }

            @Override
            public Dob[] newArray(int size) {
                return new Dob[size];
            }
        };
    }
    public static class Name implements Parcelable {
        @Expose
        @SerializedName("last")
        @ColumnInfo(name = "last_name")
        private String last;

        @Expose
        @SerializedName("first")
        @ColumnInfo(name = "first_name")
        private String first;

        @Expose
        @SerializedName("title")
        @ColumnInfo(name = "user_title")
        private String title;

        public String getLast() {
            return last;
        }

        public void setLast(String last) {
            this.last = last;
        }

        public String getFirst() {
            return first;
        }

        public void setFirst(String first) {
            this.first = first;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        @Override
        public String toString() {
            return "Name{" +
                    "last='" + last + '\'' +
                    ", first='" + first + '\'' +
                    ", title='" + title + '\'' +
                    '}';
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.last);
            dest.writeString(this.first);
            dest.writeString(this.title);
        }

        public Name() {
        }

        protected Name(Parcel in) {
            this.last = in.readString();
            this.first = in.readString();
            this.title = in.readString();
        }

        public static final Parcelable.Creator<Name> CREATOR = new Parcelable.Creator<Name>() {
            @Override
            public Name createFromParcel(Parcel source) {
                return new Name(source);
            }

            @Override
            public Name[] newArray(int size) {
                return new Name[size];
            }
        };
    }

    @Override
    public String toString() {
        return "RandomUser{" +
                "phone='" + phone + '\'' +
                ", seed=" + seed +
                ", dob=" + dob +
                ", email='" + email + '\'' +
                ", name=" + name +
                ", gender='" + gender + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.userId);
        dest.writeString(this.seed);
        dest.writeParcelable(this.name, flags);
        dest.writeParcelable(this.dob, flags);
        dest.writeString(this.phone);
        dest.writeString(this.email);
        dest.writeString(this.gender);
    }

    protected RandomUser(Parcel in) {
        this.userId = in.readInt();
        this.seed = in.readString();
        this.name = in.readParcelable(Name.class.getClassLoader());
        this.dob = in.readParcelable(Dob.class.getClassLoader());
        this.phone = in.readString();
        this.email = in.readString();
        this.gender = in.readString();
    }

    public static final Parcelable.Creator<RandomUser> CREATOR = new Parcelable.Creator<RandomUser>() {
        @Override
        public RandomUser createFromParcel(Parcel source) {
            return new RandomUser(source);
        }

        @Override
        public RandomUser[] newArray(int size) {
            return new RandomUser[size];
        }
    };
}
