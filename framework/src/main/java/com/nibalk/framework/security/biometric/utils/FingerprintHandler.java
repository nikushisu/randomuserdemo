package com.nibalk.framework.security.biometric.utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

@TargetApi(Build.VERSION_CODES.M)
public class FingerprintHandler extends FingerprintManager.AuthenticationCallback {

    private static final String TAG = "FingerprintHandler";

    private CancellationSignal cancellationSignal;
    private Context context;
    private FingerprintManagerCallbacks fpCallbacks;

    public FingerprintHandler(Context context, FingerprintManagerCallbacks fpCallbacks) {
        this.context = context;
        this.fpCallbacks = fpCallbacks;
    }

    public void startAuth(FingerprintManager manager, FingerprintManager.CryptoObject cryptoObject) {
        cancellationSignal = new CancellationSignal();
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
    }

    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
        Log.d(TAG, "Authentication error\n" + errString);
        if (fpCallbacks != null) {
            fpCallbacks.onAuthenticationError(errMsgId, errString);
        }
    }

    @Override
    public void onAuthenticationFailed() {
        Log.d(TAG, "Authentication failed");
        if (fpCallbacks != null) {
            fpCallbacks.onAuthenticationFailed();
        }
    }

    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
        Log.d(TAG, "Authentication help\n" + helpString);
        if (fpCallbacks != null) {
            fpCallbacks.onAuthenticationHelp(helpMsgId, helpString);
        }
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        Log.d(TAG, "Authentication successful");
        if (fpCallbacks != null) {
            fpCallbacks.onAuthenticationSucceeded(result);
        }

    }

}
