package com.nibalk.framework.security.di;


import com.nibalk.framework.security.biometric.manager.BiometricManager;
import com.nibalk.framework.security.biometric.manager.BiometricManagerImpl;
import com.nibalk.framework.security.encryption.manager.EncryptionManager;
import com.nibalk.framework.security.encryption.manager.EncryptionManagerImpl;
import com.nibalk.framework.security.encryption.manager.EncryptionRandomKeyManager;
import com.nibalk.framework.security.encryption.manager.EncryptionRandomKeyManagerImpl;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static com.nibalk.framework.base.Constants.Security.ENCRYPTION_MANAGER_DEFAULT;
import static com.nibalk.framework.base.Constants.Security.ENCRYPTION_MANAGER_WITH_KEY;


@Module
public class SecurityModule {

    @Singleton
    @Provides
    @Named(ENCRYPTION_MANAGER_DEFAULT)
    EncryptionManager provideEncryptionManagerDefault() {
        return new EncryptionManagerImpl();
    }

    @Singleton
    @Provides
    @Named(ENCRYPTION_MANAGER_WITH_KEY)
    EncryptionManager provideEncryptionManager(EncryptionRandomKeyManager randomKeyManager) {
        return new EncryptionManagerImpl(randomKeyManager);
    }

    @Singleton
    @Provides
    EncryptionRandomKeyManager provideEncryptionRandomKeyManager() {
        return new EncryptionRandomKeyManagerImpl();
    }

    @Singleton
    @Provides
    BiometricManager provideBiometricManager() {
        return new BiometricManagerImpl();
    }

}
