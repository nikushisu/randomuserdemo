package com.nibalk.framework.security.encryption.manager;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.nibalk.framework.security.utils.SecurityUtils;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import static com.nibalk.framework.base.Constants.Security.NUM_BITS;
import static com.nibalk.framework.base.Constants.Security.RADIX;
import static com.nibalk.framework.base.Constants.Security.TRANSFORMATION_ALGORITHM;


public class EncryptionManagerImpl implements EncryptionManager {

    private static final String TAG = "EncryptionManager";

    private Cipher encryptCipher;
    private Cipher decryptCipher;

    private EncryptionRandomKeyManager randomKeyManager;

    public EncryptionManagerImpl() {
        this(null);
    }

    public EncryptionManagerImpl(EncryptionRandomKeyManager randomKeyManager) {
        this.randomKeyManager = randomKeyManager;

        String randomKey = "";

        if (this.randomKeyManager != null) {
            randomKey = this.randomKeyManager.getRandomKey();
            Log.d(TAG, "RandomKeyManager is available (Key = )" + randomKey);
        }

        if (TextUtils.isEmpty(randomKey)) {
            randomKey = generateSecuredKey(NUM_BITS, RADIX);
            Log.d(TAG,"RandomKey is empty so generating again (Key = )" + randomKey);
        }

        if (this.randomKeyManager != null) {
            this.randomKeyManager.setRandomKey(randomKey);
            Log.d(TAG,"RandomKeyManager is available so storing (Key = )" + randomKey);
        } else {
            Log.d(TAG,"RandomKeyManager is not available so calling init (Key = )" + randomKey);
            init(randomKey);
        }
    }

    @Override
    public void init(String randomKey) {
        try {
            initCiphers(randomKey);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException |
                InvalidAlgorithmParameterException  exception) {

            Log.d(TAG, exception.getMessage());
            throw SecurityUtils.mapEncryptionExceptions(exception);
        }
    }


    @Override
    public String encrypt(String plainText) {
        String securedText = "";
        try {
            if (encryptCipher != null) {
                byte[] encryptedValue = encryptCipher.doFinal(plainText.getBytes(StandardCharsets.UTF_8));
                securedText = Base64.encodeToString(encryptedValue, Base64.NO_WRAP);
            }
        } catch (IllegalBlockSizeException | BadPaddingException exception) {
            Log.d(TAG, exception.getMessage());
            throw SecurityUtils.mapEncryptionExceptions(exception);
        }
        return securedText;
    }

    @Override
    public String decrypt(String encryptedText) {
        String plainText = "";
        try {
            if (decryptCipher != null) {
                byte[] securedText = Base64.decode(encryptedText, Base64.NO_WRAP);
                byte[] decryptedValue = decryptCipher.doFinal(securedText);
                plainText = new String(decryptedValue);
            }
        } catch (IllegalBlockSizeException | BadPaddingException exception) {
            Log.d(TAG, exception.getMessage());
            throw SecurityUtils.mapEncryptionExceptions(exception);
        }
        return plainText;
    }

    private void initCiphers(String secureRandomKey) throws NoSuchAlgorithmException,
            NoSuchPaddingException,
            InvalidKeyException,
            InvalidAlgorithmParameterException {

        int blockSize = 0;
        IvParameterSpec ivParameterSpec;
        SecretKeySpec secretKeySpec;

        this.encryptCipher = Cipher.getInstance(TRANSFORMATION_ALGORITHM);
        if (this.encryptCipher != null) {
            blockSize = this.encryptCipher.getBlockSize();
            ivParameterSpec = getIvParameterSpec(secureRandomKey, blockSize);
            secretKeySpec = getSecretKeySpec(secureRandomKey, blockSize);
            encryptCipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
        }

        this.decryptCipher = Cipher.getInstance(TRANSFORMATION_ALGORITHM);
        if (this.decryptCipher != null) {
            blockSize = this.decryptCipher.getBlockSize();
            ivParameterSpec = getIvParameterSpec(secureRandomKey, blockSize);
            secretKeySpec = getSecretKeySpec(secureRandomKey, blockSize);
            this.decryptCipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);
        }
    }

    @NonNull
    private String generateSecuredKey(int numBits, int radix) {
        SecureRandom secureRandom = new SecureRandom();
        BigInteger randomInteger = new BigInteger(numBits, secureRandom);
        return randomInteger.toString(radix);
    }

    @NonNull
    private IvParameterSpec getIvParameterSpec(String secureRandomKey, int blockSize) {
        byte[] iv = new byte[blockSize];
        System.arraycopy(secureRandomKey.getBytes(), 0, iv, 0, blockSize);
        return new IvParameterSpec(iv);
    }

    @NonNull
    private SecretKeySpec getSecretKeySpec(String secureRandomKey,
                                           int blockSize) throws NoSuchAlgorithmException {
        byte[] keyBytes = createSecureKeyBytes(secureRandomKey, blockSize);
        return new SecretKeySpec(keyBytes, TRANSFORMATION_ALGORITHM);
    }

    private byte[] createSecureKeyBytes(String secureRandomKey,
                                        int blockSize) throws NoSuchAlgorithmException {
        byte[] keyBytes = new byte[blockSize];
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(secureRandomKey.getBytes());
        System.arraycopy(md.digest(), 0, keyBytes, 0, keyBytes.length);
        return keyBytes;
    }

}
