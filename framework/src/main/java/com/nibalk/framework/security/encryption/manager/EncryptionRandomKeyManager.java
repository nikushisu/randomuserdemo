package com.nibalk.framework.security.encryption.manager;


public interface EncryptionRandomKeyManager {

    void setRandomKey(String randomKey);

    String getRandomKey();
}
