package com.nibalk.framework.security.encryption.manager;

public interface EncryptionManager {

    void init(String randomKey);

    String encrypt(String plainText);

    String decrypt(String encryptedText);
}
