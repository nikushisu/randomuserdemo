package com.nibalk.framework.security.encryption.manager;



public class EncryptionRandomKeyManagerImpl implements EncryptionRandomKeyManager {

    private String randomKey;

    public  EncryptionRandomKeyManagerImpl () {}

    @Override
    public void setRandomKey(String randomKey) {
        this.randomKey = randomKey;
    }

    @Override
    public String getRandomKey() {
        return randomKey;
    }
}
