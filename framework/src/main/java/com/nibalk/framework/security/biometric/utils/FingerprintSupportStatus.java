package com.nibalk.framework.security.biometric.utils;

public enum FingerprintSupportStatus {
    FP_SUPPORTED,
    FP_NOT_SUPPORTED,
    FP_HARDWARE_NOT_SUPPORTED,
    FP_PERMISSION_NOT_GRANTED,
    FP_HAS_NOT_REGISTERED,
    FP_LOCK_SCREEN_NOT_SECURED
}
