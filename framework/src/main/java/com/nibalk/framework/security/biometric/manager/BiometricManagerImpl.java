package com.nibalk.framework.security.biometric.manager;

import android.Manifest;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;

import com.nibalk.framework.security.biometric.utils.FingerprintHandler;
import com.nibalk.framework.security.biometric.utils.FingerprintManagerCallbacks;
import com.nibalk.framework.security.biometric.utils.FingerprintSupportStatus;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import static android.content.Context.FINGERPRINT_SERVICE;
import static android.content.Context.KEYGUARD_SERVICE;
import static com.nibalk.framework.base.Constants.Security.KEY_FOR_FP_AUTH;
import static com.nibalk.framework.security.biometric.utils.FingerprintSupportStatus.FP_HARDWARE_NOT_SUPPORTED;
import static com.nibalk.framework.security.biometric.utils.FingerprintSupportStatus.FP_HAS_NOT_REGISTERED;
import static com.nibalk.framework.security.biometric.utils.FingerprintSupportStatus.FP_LOCK_SCREEN_NOT_SECURED;
import static com.nibalk.framework.security.biometric.utils.FingerprintSupportStatus.FP_NOT_SUPPORTED;
import static com.nibalk.framework.security.biometric.utils.FingerprintSupportStatus.FP_PERMISSION_NOT_GRANTED;
import static com.nibalk.framework.security.biometric.utils.FingerprintSupportStatus.FP_SUPPORTED;

public class BiometricManagerImpl implements BiometricManager {

    private Cipher cipher;
    private KeyStore keyStore;
    private KeyGenerator keyGenerator;
    private FingerprintManager.CryptoObject cryptoObject;
    private FingerprintManager fingerprintManager;
    private KeyguardManager keyguardManager;

    @Override
    @RequiresApi(api = Build.VERSION_CODES.M)
    public FingerprintSupportStatus isDeviceSupportedFingerprint(Context context) {

        try {
            keyguardManager = (KeyguardManager) context.getSystemService(KEYGUARD_SERVICE);
            fingerprintManager = (FingerprintManager) context.getSystemService(FINGERPRINT_SERVICE);

            // Check if sensor for FP is available
            if (!fingerprintManager.isHardwareDetected()) {
                return FP_HARDWARE_NOT_SUPPORTED;
            }
            // Check if permission for FP is granted
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) !=
                    PackageManager.PERMISSION_GRANTED) {
                return FP_PERMISSION_NOT_GRANTED;
            }
            // Check if at least one FP is registered in device
            if (!fingerprintManager.hasEnrolledFingerprints()) {
                return FP_HAS_NOT_REGISTERED;
            }
            // Check that the lock-screen is secured with passcode etc.
            if (!keyguardManager.isKeyguardSecure()) {
                return FP_LOCK_SCREEN_NOT_SECURED;
            }
        } catch (Exception e) {
            return FP_NOT_SUPPORTED;
        }

        return FP_SUPPORTED;
    }

    public void startFingerprintAuthentication(Context context, FingerprintManagerCallbacks callbacks) {
        generateKey();

        if (initCipher()) {
            cryptoObject = new FingerprintManager.CryptoObject(cipher);
            FingerprintHandler helper = new FingerprintHandler(context, callbacks);
            helper.startAuth(fingerprintManager, cryptoObject);
        }
    }

    private void generateKey()  {
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
            keyStore.load(null);
            keyGenerator.init(new KeyGenParameterSpec.Builder(KEY_FOR_FP_AUTH,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());

            keyGenerator.generateKey();
        } catch (KeyStoreException
                | NoSuchAlgorithmException
                | NoSuchProviderException
                | InvalidAlgorithmParameterException
                | CertificateException
                | IOException e) {
            throw new RuntimeException("Failed to generate key", e);
        }
    }

    private boolean initCipher() {
        try {
            cipher = Cipher.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES + "/"
                            + KeyProperties.BLOCK_MODE_CBC + "/"
                            + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException |
                NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }
        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_FOR_FP_AUTH,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException | CertificateException
                | UnrecoverableKeyException | IOException
                | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }
}
