package com.nibalk.framework.security.utils;

import com.nibalk.framework.base.Constants;
import com.nibalk.framework.base.error.BaseException;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.NoSuchPaddingException;

public class SecurityUtils {



    public static BaseException mapEncryptionExceptions(Throwable exception) {
        String errorCode = "";

        if (exception instanceof NoSuchAlgorithmException) {
            errorCode = Constants.ErrorCode.SECURITY_NO_SUCH_ALGORITHM_EXCEPTION;
        } else if (exception instanceof NoSuchProviderException) {
            errorCode = Constants.ErrorCode.SECURITY_NO_SUCH_PROVIDER_EXCEPTION;
        } else if (exception instanceof NoSuchPaddingException) {
            errorCode = Constants.ErrorCode.SECURITY_NO_SUCH_PADDING_EXCEPTION;
        } else if (exception instanceof InvalidKeyException) {
            errorCode = Constants.ErrorCode.SECURITY_INVALID_KEY_EXCEPTION;
        } else if (exception instanceof InvalidAlgorithmParameterException) {
            errorCode = Constants.ErrorCode.SECURITY_INVALID_ALGO_PARAM_EXCEPTION;
        } else if (exception instanceof UnsupportedEncodingException) {
            errorCode = Constants.ErrorCode.SECURITY_UNSUPPORTED_ENCODING_EXCEPTION;
        }

        return new BaseException(errorCode, exception);
    }
}
