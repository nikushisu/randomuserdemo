package com.nibalk.framework.security.biometric.manager;

import android.content.Context;

import com.nibalk.framework.security.biometric.utils.FingerprintManagerCallbacks;
import com.nibalk.framework.security.biometric.utils.FingerprintSupportStatus;

public interface BiometricManager {

    FingerprintSupportStatus isDeviceSupportedFingerprint(Context context);

    void startFingerprintAuthentication(Context context, FingerprintManagerCallbacks callbacks);
}
