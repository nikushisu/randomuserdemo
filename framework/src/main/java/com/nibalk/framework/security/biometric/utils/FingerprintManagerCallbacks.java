package com.nibalk.framework.security.biometric.utils;

import android.hardware.fingerprint.FingerprintManager;

public interface FingerprintManagerCallbacks {

    void onAuthenticationError(int errorCode, CharSequence errString);
    void onAuthenticationHelp(int helpCode, CharSequence helpString);
    void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result);
    void onAuthenticationFailed();
}
