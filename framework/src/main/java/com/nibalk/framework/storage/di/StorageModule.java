package com.nibalk.framework.storage.di;

import android.content.Context;
import android.support.annotation.NonNull;

import com.nibalk.framework.security.encryption.manager.EncryptionManager;
import com.nibalk.framework.security.encryption.manager.EncryptionRandomKeyManager;
import com.nibalk.framework.storage.roomdb.manager.RoomDbManager;
import com.nibalk.framework.storage.roomdb.manager.RoomDbManagerImpl;
import com.nibalk.framework.storage.sharedpref.manager.SharedPrefManager;
import com.nibalk.framework.storage.sharedpref.manager.SharedPrefManagerImpl;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static com.nibalk.framework.base.Constants.Security.ENCRYPTION_MANAGER_WITH_KEY;


@Module
public class StorageModule {

    @Singleton
    @Provides
    SharedPrefManager provideSharedPrefManager(@NonNull Context context,
                                               @Named(ENCRYPTION_MANAGER_WITH_KEY) EncryptionManager encryptionManager,
                                               EncryptionRandomKeyManager randomKeyManager) {
        return new SharedPrefManagerImpl(context, encryptionManager, randomKeyManager);
    }

    @Singleton
    @Provides
    RoomDbManager provideRoomDbManager(SharedPrefManager sharedPrefManager,
                                       @Named(ENCRYPTION_MANAGER_WITH_KEY) EncryptionManager encryptionManager) {
        return new RoomDbManagerImpl(sharedPrefManager, encryptionManager);
    }
}
