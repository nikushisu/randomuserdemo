package com.nibalk.framework.storage.roomdb.manager;

import com.nibalk.framework.security.encryption.manager.EncryptionManager;
import com.nibalk.framework.storage.sharedpref.manager.SharedPrefManager;

import javax.inject.Named;

import static com.nibalk.framework.base.Constants.Security.ENCRYPTION_MANAGER_WITH_KEY;

public class RoomDbManagerImpl implements RoomDbManager {

    private SharedPrefManager sharedPrefManager;
    private EncryptionManager encryptionManager;

    public RoomDbManagerImpl(SharedPrefManager SharedPrefManager,
                             @Named(ENCRYPTION_MANAGER_WITH_KEY) EncryptionManager encryptionManager) {
        this.sharedPrefManager = sharedPrefManager;
        this.encryptionManager = encryptionManager;
    }

    @Override
    public String encryptedValue(String plainText) {
        return encryptionManager.encrypt(plainText);
    }

    @Override
    public String decryptedValue(String encryptedText) {
        return encryptionManager.decrypt(encryptedText);
    }
}
