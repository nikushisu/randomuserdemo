package com.nibalk.framework.storage.sharedpref.manager;

import org.json.JSONArray;
import org.json.JSONObject;


public interface SharedPrefManager {

    void storeItem(String key, int value);
    void storeItem(String key, float value);
    void storeItem(String key, double value);
    void storeItem(String key, long value);
    void storeItem(String key, boolean value);
    void storeItem(String key, String value);
    void storeItem(String key, JSONObject value);
    void storeItem(String key, JSONArray value);

    int retrieveItem(String key, int defaultValue);
    float retrieveItem(String key, float defaultValue);
    double retrieveItem(String key, double defaultValue);
    long retrieveItem(String key, long defaultValue);
    boolean retrieveItem(String key, boolean defaultValue);
    String retrieveItem(String key, String defaultValue);
    JSONObject retrieveItem(String key, JSONObject defaultValue);
    JSONArray retrieveItem(String key, JSONArray defaultValue);
}
