package com.nibalk.framework.storage.sharedpref.manager;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;

import com.nibalk.framework.security.encryption.manager.EncryptionManager;
import com.nibalk.framework.security.encryption.manager.EncryptionRandomKeyManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.inject.Named;

import static com.nibalk.framework.base.Constants.Security.ENCRYPTION_MANAGER_WITH_KEY;
import static com.nibalk.framework.base.Constants.Storage.KEY_VAL_STORE_KEY;
import static com.nibalk.framework.base.Constants.Storage.KEY_VAL_STORE_NAME;


public class SharedPrefManagerImpl  implements SharedPrefManager {

    private static final String TAG = "SharedPrefManager";


    private SharedPreferences keyValueStore;
    private EncryptionManager encryptionManager;

    public SharedPrefManagerImpl(@NonNull Context context,
                                 @Named(ENCRYPTION_MANAGER_WITH_KEY) EncryptionManager encryptionManager,
                                 EncryptionRandomKeyManager randomKeyManager) {
        String storeKey = "";

        keyValueStore = context.getSharedPreferences(KEY_VAL_STORE_NAME, Context.MODE_PRIVATE);

        if (keyValueStore.contains(KEY_VAL_STORE_KEY)) {
            Log.d(TAG, "Key - Already Stored");

            storeKey = keyValueStore.getString(KEY_VAL_STORE_KEY, ""); //-- use stored key
            randomKeyManager.setRandomKey(storeKey);

            this.encryptionManager = encryptionManager;
            this.encryptionManager.init(storeKey);

        } else {
            Log.d(TAG, "Key - Need to store");

            storeKey = randomKeyManager.getRandomKey();
            keyValueStore.edit().putString(KEY_VAL_STORE_KEY, storeKey).apply(); //-- store new key

            this.encryptionManager = encryptionManager;
            this.encryptionManager.init(storeKey);
        }
    }

    private String encryptedValue(String planText) {
        String value = encryptionManager.encrypt(planText);
        return value;
    }

    @Override
    public void storeItem(String key, int value) {

    }

    @Override
    public void storeItem(String key, float value) {

    }

    @Override
    public void storeItem(String key, double value) {

    }

    @Override
    public void storeItem(String key, long value) {

    }

    @Override
    public void storeItem(String key, boolean value) {

    }

    @Override
    public void storeItem(String key, String value) {
        String encryptedValue = encryptedValue(value);
        keyValueStore.edit().putString(key, encryptedValue).apply();
        Log.d(TAG , encryptedValue);
    }

    @Override
    public void storeItem(String key, JSONObject value) {

    }

    @Override
    public void storeItem(String key, JSONArray value) {

    }

    @Override
    public int retrieveItem(String key, int defaultValue) {
        return 0;
    }

    @Override
    public float retrieveItem(String key, float defaultValue) {
        return 0;
    }

    @Override
    public double retrieveItem(String key, double defaultValue) {
        return 0;
    }

    @Override
    public long retrieveItem(String key, long defaultValue) {
        return 0;
    }

    @Override
    public boolean retrieveItem(String key, boolean defaultValue) {
        return false;
    }

    @Override
    public String retrieveItem(String key, String defaultValue) {
        return null;
    }

    @Override
    public JSONObject retrieveItem(String key, JSONObject defaultValue) {
        return null;
    }

    @Override
    public JSONArray retrieveItem(String key, JSONArray defaultValue) {
        return null;
    }
}
