package com.nibalk.framework.storage.roomdb.manager;

public interface RoomDbManager {

    String encryptedValue(String plainText);

    String decryptedValue(String encryptedText);
}
