package com.nibalk.framework.base.di;

import com.nibalk.framework.network.di.NetworkModule;
import com.nibalk.framework.security.di.SecurityModule;
import com.nibalk.framework.storage.di.StorageModule;

import dagger.Module;

@Module(includes = {NetworkModule.class, SecurityModule.class, StorageModule.class})
public class FrameworkModule {


}
