package com.nibalk.framework.base;

public class Constants {

    public static class Network {
        public static final String BASE_URL_KEY = "base_url";
    }
    public static class Security {
        public static final int NUM_BITS = 130;
        public static final int RADIX = 32;
        public static final String TRANSFORMATION_ALGORITHM = "AES/CBC/PKCS5Padding";
        public static final String ENCRYPTION_MANAGER_WITH_KEY = "ENCRYPTION_MANAGER_WITH_KEY";
        public static final String ENCRYPTION_MANAGER_DEFAULT = "ENCRYPTION_MANAGER_DEFAULT";
        public static final String KEY_FOR_FP_AUTH = "KEY_FOR_FP_AUTH";
    }

    public static class Storage {
        public static final String KEY_VAL_STORE_NAME = "KeyValueStore";
        public static final String KEY_VAL_STORE_KEY = "sOmEkEy4stOrAgE2sEcUrEdAtA";
    }

    public static class ErrorCode {
        public static final String NETWORK_NO_NETWORK_EXCEPTION = "NETWORK_NO_NETWORK_EXCEPTION";
        public static final String NETWORK_CONNECT_EXCEPTION = "NETWORK_CONNECT_EXCEPTION";
        public static final String NETWORK_UNKNOWN_HOST_EXCEPTION = "NETWORK_UNKNOWN_HOST_EXCEPTION";
        public static final String NETWORK_SOCKET_EXCEPTION = "NETWORK_SOCKET_EXCEPTION";
        public static final String NETWORK_SOCKET_TIMEOUT_EXCEPTION = "NETWORK_SOCKET_TIMEOUT_EXCEPTION";
        public static final String NETWORK_MALFORMED_JSON_EXCEPTION = "NETWORK_MALFORMED_JSON_EXCEPTION";

        public static final String HTTP_CLIENT_INVALID_REQUEST_EXCEPTION = "ERROR_400";
        public static final String HTTP_CLIENT_ACCESS_FORBIDDEN_EXCEPTION = "ERROR_403";
        public static final String HTTP_CLIENT_RESOURCE_NOT_FOUND_EXCEPTION = "ERROR_404";
        public static final String HTTP_CLIENT_REQUEST_METHOD_NOT_SUPPORT_EXCEPTION = "ERROR_405";
        public static final String HTTP_SERVER_INTERNAL_SERVER_ERROR_EXCEPTION = "ERROR_500";

        public static final String SECURITY_NO_SUCH_ALGORITHM_EXCEPTION = "SECURITY_NO_SUCH_ALGORITHM_EXCEPTION";
        public static final String SECURITY_NO_SUCH_PROVIDER_EXCEPTION = "SECURITY_NO_SUCH_PROVIDER_EXCEPTION";
        public static final String SECURITY_NO_SUCH_PADDING_EXCEPTION = "SECURITY_NO_SUCH_PADDING_EXCEPTION";
        public static final String SECURITY_INVALID_KEY_EXCEPTION = "SECURITY_INVALID_KEY_EXCEPTION";
        public static final String SECURITY_INVALID_ALGO_PARAM_EXCEPTION = "SECURITY_INVALID_ALGO_PARAM_EXCEPTION";
        public static final String SECURITY_UNSUPPORTED_ENCODING_EXCEPTION = "SECURITY_UNSUPPORTED_ENCODING_EXCEPTION";
    }
}
