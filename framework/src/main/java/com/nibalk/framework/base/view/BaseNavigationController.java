package com.nibalk.framework.base.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

public class BaseNavigationController {

    private Activity context;
    private FragmentManager fragmentManager;

    public BaseNavigationController() {}

    public BaseNavigationController(Activity activity, FragmentManager fragmentManager) {
        this.context = activity;
        this.fragmentManager = fragmentManager;
    }

    public void addFragment(Bundle bundle, Fragment fragment, Class fragmentClass, int containerId) {

        if (bundle != null) {
            fragment.setArguments(bundle);
        }

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(containerId, fragment, fragmentClass.getSimpleName());
        fragmentTransaction.addToBackStack(fragmentClass.getSimpleName());
        fragmentTransaction.commit();
    }

    public void replaceFragment(Bundle bundle, Fragment fragment, Class fragmentClass, int containerId) {

        if (bundle != null) {
            fragment.setArguments(bundle);
        }

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(containerId, fragment, fragmentClass.getSimpleName());
        //fragmentTransaction.addToBackStack(fragmentClass.getSimpleName());
        fragmentTransaction.commit();
    }
}
