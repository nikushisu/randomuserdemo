package com.nibalk.framework.base.view;

import android.arch.lifecycle.ViewModel;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import dagger.android.AndroidInjection;

public abstract class BaseActivity<VM extends ViewModel, VDB extends ViewDataBinding> extends AppCompatActivity {

    private static final String TAG = "BaseActivity";

    private VM viewModel;
    private VDB dataBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate()");

        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);

        this.viewModel = this.viewModel != null ? this.viewModel : setupViewModel();

        try {
            this.dataBinding = setupDataBinding();
            Log.d(TAG, "Data binding - success");
        } catch (Exception e) {
            Log.d(TAG, "Data binding - fail");
        }
    }

    protected abstract VM setupViewModel();
    protected abstract VDB setupDataBinding();

    protected VM getViewModel() {
        return viewModel;
    }
    protected VDB getDataBinding() {
        return dataBinding;
    }


}
