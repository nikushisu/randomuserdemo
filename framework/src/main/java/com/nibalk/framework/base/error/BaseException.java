package com.nibalk.framework.base.error;

public class BaseException extends RuntimeException {

    private String errorCode;
    private String errorTitle;
    private String errorMessage;
    private Throwable throwable;

    public BaseException() {}

    public BaseException(String errorCode) {
        this.errorCode = errorCode;
    }

    public BaseException(String errorCode, String errorTitle, String errorMessage) {
        this.errorCode = errorCode;
        this.errorTitle = errorTitle;
        this.errorMessage = errorMessage;
    }

    public BaseException(String errorTitle, String errorMessage) {
        this.errorCode = errorCode;
        this.errorTitle = errorTitle;
        this.errorMessage = errorMessage;
    }

    public BaseException(String errorCode, Throwable throwable) {
        this.errorCode = errorCode;
        this.throwable = throwable;
    }

    public BaseException(Throwable throwable) {
        this.throwable = throwable;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorTitle() {
        return errorTitle;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    @Override
    public String toString() {
        return "BaseException{" +
                "errorCode='" + errorCode + '\'' +
                ", errorTitle='" + errorTitle + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                ", throwable=" + throwable +
                '}';
    }
}
