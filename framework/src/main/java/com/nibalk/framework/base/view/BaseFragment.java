package com.nibalk.framework.base.view;

import android.arch.lifecycle.ViewModel;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.animation.AlphaAnimation;
import android.widget.FrameLayout;

import dagger.android.support.AndroidSupportInjection;

public abstract class BaseFragment<VM extends ViewModel, VDB extends ViewDataBinding> extends Fragment {

    private static final String TAG = "BaseFragment";

    private VM viewModel;
    private VDB dataBinding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate()");

        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);

        this.viewModel = this.viewModel != null ? this.viewModel : setupViewModel();

        try {
            this.dataBinding = setupDataBinding();
            Log.d(TAG, "Data binding - success");
        } catch (Exception e) {
            Log.d(TAG, "Data binding - fail");
        }
    }

    protected abstract VM setupViewModel();
    protected abstract VDB setupDataBinding();

    protected VM getViewModel() {
        return viewModel;
    }
    protected VDB getDataBinding() {
        return dataBinding;
    }

    protected void setProgressBarVisibility(FrameLayout progressBar, float fromAlpha, float toAlpha,
                                          long duration, int visibility) {
        AlphaAnimation inAnimation = new AlphaAnimation(fromAlpha, toAlpha);
        inAnimation.setDuration(duration);

        progressBar.setAnimation(inAnimation);
        progressBar.setVisibility(visibility);
    }


}
