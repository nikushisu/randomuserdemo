package com.nibalk.framework.base;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

    private static final String TAG = "DateUtils";

    public static String convertOneDateFormatToAnother(String dateToBeConverted,
                                                     String originalFormat, String targetFormat, Locale locale) {
        String formattedDate = dateToBeConverted;
        try {
            DateFormat originalDateFormat = new SimpleDateFormat(originalFormat, locale);
            DateFormat targetDateFormat = new SimpleDateFormat(targetFormat, locale);
            Date date = originalDateFormat.parse(dateToBeConverted);
            formattedDate = targetDateFormat.format(date);
            Log.d(TAG, "getDateFromString() - formattedDate ->" + formattedDate);


        } catch (ParseException e) {
            Log.d(TAG, "getDateFromString() - Error passing date ->" + e.getMessage());
        }
        return formattedDate;
    }


    public static Date getDateFromString(String requiredDateString, String dateFormat, Locale locale) {
        Log.d(TAG, "getDateFromString()");

        Date date = null;
        try {
            date = new SimpleDateFormat(dateFormat, locale).parse(requiredDateString);
            Log.d(TAG, "getDateFromString() - formattedDate ->" + date);

        } catch (ParseException e) {
            Log.d(TAG, "getDateFromString() - Error passing date." + e.getMessage());
        }
        return date;
    }

    public static String getFormattedDate(Date requiredDate, String dateFormat, Locale locale) {
        Log.d(TAG, "getFormattedDate()");

        return (new SimpleDateFormat(dateFormat, locale)).format(requiredDate);
    }
}
