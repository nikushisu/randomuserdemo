package com.nibalk.framework.network.manager;

import android.net.ConnectivityManager;
import android.util.Log;

import com.nibalk.framework.base.error.BaseException;
import com.nibalk.framework.network.utils.NetworkUtils;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.Response;

import static com.nibalk.framework.base.Constants.ErrorCode.NETWORK_NO_NETWORK_EXCEPTION;

public class NetworkManagerImpl implements NetworkManager {

    private static final String TAG = "NetworkManager";

    private ConnectivityManager connectivityManager;

    public NetworkManagerImpl(ConnectivityManager connectivityManager) {
        this.connectivityManager = connectivityManager;
    }

    @Override
    public <T> Observable<T> execute(final Call<T> call) {
        return Observable.just(NetworkUtils.isInternetAvailable(connectivityManager))
                .flatMap(available -> {
                    if (available) {
                        Log.d(TAG, "Network Available");
                        return executeServiceCall(call);
                    } else {
                        Log.d(TAG,"Network Not Available");
                        throw new BaseException(NETWORK_NO_NETWORK_EXCEPTION, "Error", "Network Not Available");
                        //return Observable.error(new BaseException(NETWORK_NO_NETWORK_EXCEPTION, "Error", "Network Not Available"));
                    }
                });
    }

    private <T> Observable<T> executeServiceCall(final Call<T> call) {
        Log.d(TAG,"executeServiceCall()");

        Observable<T> observable = Observable.fromCallable(() -> {
            try {
                Response<T> response = call.execute();

                if (response.isSuccessful()) {
                    Log.d(TAG,"Service call execution is successful");
                    return response.body();
                } else {
                    Log.d(TAG,"Service call execution is unsuccessful -> " + response.code());
                    throw NetworkUtils.mapWebServiceException(response);
                }
            } catch (Exception e) {
                Log.d(TAG,"Service call execution is unsuccessful -> " + e.getMessage());
                throw NetworkUtils.mapConnectionExceptions(e);
            }
        });

        return observable;
    }
}
