package com.nibalk.framework.network.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.MalformedJsonException;

import com.nibalk.framework.base.Constants;
import com.nibalk.framework.base.error.BaseException;

import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import retrofit2.Response;

public class NetworkUtils {


    public static boolean isInternetAvailable(ConnectivityManager connectivityManager) {
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        boolean isCurrentlyConnected = networkInfo != null && networkInfo.isConnected();

        return isCurrentlyConnected;
    }

    public static BaseException mapConnectionExceptions(Throwable exception) {
        String errorCode = "";

        if (exception instanceof ConnectException) {
            errorCode = Constants.ErrorCode.NETWORK_CONNECT_EXCEPTION;
        } else if (exception instanceof UnknownHostException) {
            errorCode = Constants.ErrorCode.NETWORK_UNKNOWN_HOST_EXCEPTION;
        } else if (exception instanceof SocketException) {
            errorCode = Constants.ErrorCode.NETWORK_SOCKET_EXCEPTION;
        } else if (exception instanceof SocketTimeoutException) {
            errorCode = Constants.ErrorCode.NETWORK_SOCKET_TIMEOUT_EXCEPTION;
        } else if (exception instanceof MalformedJsonException) {
            errorCode = Constants.ErrorCode.NETWORK_MALFORMED_JSON_EXCEPTION;
        }

        return new BaseException(errorCode, exception);
    }

    public static BaseException mapWebServiceException(Response response) {
        String errorCode = "";

        switch (response.code()) {
            case 400:
                errorCode = Constants.ErrorCode.HTTP_CLIENT_INVALID_REQUEST_EXCEPTION;
                break;
            case 403:
                errorCode = Constants.ErrorCode.HTTP_CLIENT_ACCESS_FORBIDDEN_EXCEPTION;
                break;
            case 404:
                errorCode = Constants.ErrorCode.HTTP_CLIENT_RESOURCE_NOT_FOUND_EXCEPTION;
                break;
            case 405:
                errorCode = Constants.ErrorCode.HTTP_CLIENT_REQUEST_METHOD_NOT_SUPPORT_EXCEPTION;
                break;
            case 500:
                errorCode = Constants.ErrorCode.HTTP_SERVER_INTERNAL_SERVER_ERROR_EXCEPTION;
                break;
            default:
                errorCode = String.valueOf(response.code());
                break;
        }

        return new BaseException(errorCode);
    }
}
